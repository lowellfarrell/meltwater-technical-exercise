function redactHTML() {
    const censor_params = document.getElementById('censor_params').value;
    const file_text = document.getElementById('file_text').value;

    document.getElementById('redacted_text').value = redact(censor_params, file_text);
}

function redact(censor_params, file_text) {
    let redactions = [];
    if (censor_params) {
        let str = censor_params;
        const regexp = /"([^"]*)"|'([^']*)'|[^\s,.!?;:]+/g;
        redactions = findRegexMatches(str, regexp);

        if (redactions.length > 1) {
            redactions.sort(function (a, b) {
                return b.length - a.length;
            });
        }
    }
    let redacted_text = file_text;
    redactions.forEach(element => redacted_text = redacted_text.replaceAll(element, 'XXXXXX'));


    return redacted_text;
}

function findRegexMatches(str, reg) {
    let arr = str.match(reg);
    if (!arr) {
        arr = [];
    }

    return arr;
}
